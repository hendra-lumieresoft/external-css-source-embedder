/**
 
The MIT License (MIT)

Copyright (c) 2015 Hendra Mulyana (Lumieresoft)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

*/

cssEmbed = {
  load: function(file){
    $("link[rel='stylesheet']").each(function(index,e){
      if(typeof file === 'undefined' || $(e).attr("href").indexOf(file) > -1){
        $.ajax({
          url: $(e).attr("href"),
          async: false,
          context: cssEmbed
        }).done(function(css) {
          var id = "css-embed-" + Object.keys(this.storage).length;
          var style = "<style type='text/css' id='" + id + "'>" + this.cssURL($(e).attr("href"), css) + "</style>";
          this.storage[id] = $(e).attr("href");
          $(e).remove();
          $("head").append(style);
        });
      }
    });
    this.fixSrc();
    return this;
  },
  revert: function(file){
    for(var keys in this.storage){
      if(typeof file === 'undefined' || this.storage[keys].indexOf(file) > -1){
        var link = "<link rel='stylesheet' type='text/css' href='" + this.storage[keys] + "' />";
        delete this.storage[keys];
        $("#" + keys).remove();
        $("head").append(link);
      }
    }
    return this;
  },
  storage: {},
  cssURL: function(base, css){
    var pos = 0;
    var context = "";
    if(base.substr(0,3).indexOf("../") != -1) context = window.location.pathname.substr(0,window.location.pathname.lastIndexOf("/"));
    while(true){
      pos = css.indexOf("url(", pos);
      if(pos == -1) return(css);
      pos += 5;
      if(css.substr(pos,5).indexOf("http") == -1){
        css = [css.slice(0, pos), window.location.origin, context, "/", base.substr(0, base.lastIndexOf("/")), "/", css.slice(pos)].join('');
      }
    }
  },
  fixSrc: function(){
    var src;
    $("img").each(function(index,e){
      if($(e).attr("src").indexOf("http") == -1){
        src = window.location.origin + window.location.pathname.substr(0,window.location.pathname.lastIndexOf("/")) + "/" + $(e).attr("src");
        $(e).attr("src", src);
      }
    });
    
    $("link[type='image/x-icon']").each(function(index,e){
      src = window.location.origin + window.location.pathname.substr(0,window.location.pathname.lastIndexOf("/")) + "/" + $(e).attr("href");
      $(e).attr("href", src);
    });
  }
};

